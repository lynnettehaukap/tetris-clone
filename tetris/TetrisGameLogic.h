//TetrisGameLogic.h
//

#include <iostream>
#include <stdlib.h>
#ifndef TETRIS_GAME_LOGIC_H
#define TETRIS_GAME_LOGIC_H
const int ROWSIZE = 20;
const int COLSIZE = 10;
const int PIECELOCATION = 8;


using namespace std;

class TetrisGameLogic{
public:
    TetrisGameLogic();
    ~TetrisGameLogic();
    void gameLoop();
    char getGameGridElem(int, int);
    void setPieceforPrint();
    void setRenderer(class TetrisRenderer&); 
    void deletePiece(); 
private:
    class TetrisUserInput* m_userInput;
    class TetrisRenderer* m_renderer;
    char** m_gameGrid;
    int m_pieceLocation[PIECELOCATION];
    int m_tempPieceLocation[PIECELOCATION];
    char m_pieceType;
    void initGameGrid();
    void addPieceToGameGrid();
    int randGamePiece();
    void rotatePieceinGameGrid();
    void verticalPieceMove();
    void initPieceLocation();
    void rotatePieceRight();
    void copyPieceLocation();
    void copyTempLocation();
    bool isGridLocationEmpty( );
    bool isPieceInBounds();
    void undoDeletePiece();
   
    
};
#endif
