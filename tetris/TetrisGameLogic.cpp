//TetrisGameLogic.cpp

#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include "TetrisGameLogic.h"
#include "TetrisUserInput.h"
#include "TetrisRenderer.h"
#include "TetrisRenderer.h"

using namespace std;

static void DebugPieceLocation(char * m_pieceLocation[] )
{

}

//
TetrisGameLogic::TetrisGameLogic()
{
    //init m_gameGrid   
    m_gameGrid = new char*[ROWSIZE];
    for( int i=0; i<ROWSIZE; ++i)
    {
        m_gameGrid[i] = new char[COLSIZE];
    }  
    initGameGrid();  

    for( int i=0; i<PIECELOCATION; ++i)
    {
        m_pieceLocation[i] = 0;
        m_tempPieceLocation[i] = 0;
    }  
}

//
TetrisGameLogic::~TetrisGameLogic()
{
    //delete m_gameGrid memory
    for( int i=0; i<ROWSIZE; ++i)
    {
        delete[] m_gameGrid[i];  
    }
    delete[] m_gameGrid;

    //delete pieceLocation memory

    delete[] m_pieceLocation; 
}

//
int TetrisGameLogic::randGamePiece()
{
    //rand generate piece 1-7
    srand(time(NULL));
    int piece = (rand() % 7) + 1;
    return piece;
}

void TetrisGameLogic::setRenderer(class TetrisRenderer& myConsoleRenderer)
{
        m_renderer = &myConsoleRenderer; 
}

//used for initial start location of a piece
void TetrisGameLogic::rotatePieceinGameGrid()
{

}

//
void TetrisGameLogic::initGameGrid()
{ 
    for(int i = 0; i < ROWSIZE; i++)
    {
        for(int j = 0; j < COLSIZE; j++)
        {
            m_gameGrid[i][j] = ' ';
        }
    }
}

//
char TetrisGameLogic::getGameGridElem(int row, int col)
{
      return m_gameGrid[row][col];
}

//
void TetrisGameLogic::initPieceLocation()
{
    int type = randGamePiece();
    int col = 4;
cout << "type: " << type << endl;

    //
    if(type == 1)
    {
        //initialize bar: I
        m_pieceLocation[0] = 0;
        m_pieceLocation[1] = 4;
        m_pieceLocation[2] = 1;
        m_pieceLocation[3] = 4;
        m_pieceLocation[4] = 2;
        m_pieceLocation[5] = 4;
        m_pieceLocation[6] = 3;
        m_pieceLocation[7] = 4;
        m_pieceType = 'I';  
    }
 
    // LeftL: X
    if(type == 2)
    {
        m_pieceLocation[0] = 1;
        m_pieceLocation[1] = 2;
        m_pieceLocation[2] = 2;
        m_pieceLocation[3] = 0;
        m_pieceLocation[4] = 2;
        m_pieceLocation[5] = 1;
        m_pieceLocation[6] = 2;
        m_pieceLocation[7] = 2;
        m_pieceType = 'X';  
    }

    //RightL: #
    if(type == 3)
    {
        m_pieceLocation[0] = 1;
        m_pieceLocation[1] = 1;
        m_pieceLocation[2] = 2;
        m_pieceLocation[3] = 1;
        m_pieceLocation[4] = 2;
        m_pieceLocation[5] = 2;
        m_pieceLocation[6] = 2;
        m_pieceLocation[7] = 3;
        m_pieceType = '#'; 
    }

    //S: $
    if(type == 4)
    {
        m_pieceLocation[0] = 1;
        m_pieceLocation[1] = 1;
        m_pieceLocation[2] = 1;
        m_pieceLocation[3] = 2;
        m_pieceLocation[4] = 2;
        m_pieceLocation[5] = 0;
        m_pieceLocation[6] = 2;
        m_pieceLocation[7] = 1;
        m_pieceType = '$';  
    }

    //Z: Z
    if(type == 5)
    {
        m_pieceLocation[0] = 1;
        m_pieceLocation[1] = 1;
        m_pieceLocation[2] = 1;
        m_pieceLocation[3] = 2;
        m_pieceLocation[4] = 2;
        m_pieceLocation[5] = 2;
        m_pieceLocation[6] = 2;
        m_pieceLocation[7] = 3;
        m_pieceType = 'Z';  
    }

    //block: &
    if(type == 6)
    {
        m_pieceLocation[0] = 1;
        m_pieceLocation[1] = 1;
        m_pieceLocation[2] = 1;
        m_pieceLocation[3] = 2;
        m_pieceLocation[4] = 2;
        m_pieceLocation[5] = 1;
        m_pieceLocation[6] = 2;
        m_pieceLocation[7] = 2;  
        m_pieceType = '&';       
    }

    //T: A 
    if(type == 7)
    {
        m_pieceLocation[0] = 1;
        m_pieceLocation[1] = 1;
        m_pieceLocation[2] = 2;
        m_pieceLocation[3] = 0;
        m_pieceLocation[4] = 2;
        m_pieceLocation[5] = 1;
        m_pieceLocation[6] = 2;
        m_pieceLocation[7] = 2; 
        m_pieceType = 'A';      
    }
}

//copy pieceLocation into temp array
void TetrisGameLogic::copyPieceLocation()
{
    for( int i=0; i<PIECELOCATION; ++i)
    {
        m_tempPieceLocation[i] = m_pieceLocation[i];
    }  
}


//copy tempLocation into piece array
void TetrisGameLogic::copyTempLocation()
{
    for( int i=0; i<PIECELOCATION; ++i)
    {
        m_pieceLocation[i] = m_tempPieceLocation[i];
    }  
}

//moves piece vertically by one row
void TetrisGameLogic::verticalPieceMove()
{
    int pieceRow;
    int validCount = 0;
    copyPieceLocation();
    for(int i = 0; i < PIECELOCATION; i += 2)
    {
        pieceRow = m_tempPieceLocation[i];
        m_tempPieceLocation[i] = ++pieceRow;
    }
    
    bool inBound = isPieceInBounds();
    bool isEmpty = isGridLocationEmpty( );
  
    if(inBound == 1 && isEmpty == 1)
    {
        deletePiece();
        copyTempLocation();
    }
}

//TODO use temp array
//returns true if piece in bounds
bool TetrisGameLogic::isPieceInBounds()
{
    int pieceRow = 0;
    int pieceCol = 1;
    while(pieceRow < PIECELOCATION)
    {
        if(m_tempPieceLocation[pieceRow] < 0 || m_tempPieceLocation[pieceRow] > ROWSIZE)
        {
            return false;
        }
        if(m_tempPieceLocation[pieceCol] < 0 || m_tempPieceLocation[pieceCol] > COLSIZE)
        {
            return false;
        }
        pieceRow += 2;
        pieceCol +=2;
    }
    return true;
}

//TODO const
bool TetrisGameLogic::isGridLocationEmpty( )
{
    int pieceRow = 0;
    int pieceCol = 1;
    deletePiece();

    while(pieceRow < PIECELOCATION)
    {
        if(m_gameGrid[m_tempPieceLocation[pieceRow]][m_tempPieceLocation[pieceCol]] != ' ')
        {
            undoDeletePiece();
            return false;
        }
        pieceRow += 2;
        pieceCol +=2;
    }
    return true;
}

//m_piece type as param
//set piece in grid for printing
void TetrisGameLogic::setPieceforPrint()
{
    int pieceRow = 0;
    int pieceCol = 1;
    while(pieceRow < PIECELOCATION)
    {
        m_gameGrid[m_pieceLocation[pieceRow]][m_pieceLocation[pieceCol]] = m_pieceType;
        pieceRow += 2;
        pieceCol +=2;
    }
}

//remove current piece from grid 
void TetrisGameLogic::deletePiece()
{
    int pieceRow = 0;
    int pieceCol = 1;
    while(pieceRow < PIECELOCATION)
    {
        m_gameGrid[m_pieceLocation[pieceRow]][m_pieceLocation[pieceCol]] = ' ';
        pieceRow += 2;
        pieceCol +=2;
    }
}

//undo deletePiece and place current piece in grid 
void TetrisGameLogic::undoDeletePiece()
{
    int pieceRow = 0;
    int pieceCol = 1;

    for(int i = 0; i < PIECELOCATION; i += 2)
    {
        int rowData = m_tempPieceLocation[i];
        m_tempPieceLocation[i] = --rowData;
    }

    while(pieceRow < PIECELOCATION)
    {
        m_gameGrid[m_pieceLocation[pieceRow]][m_pieceLocation[pieceCol]] = m_pieceType;
        pieceRow += 2;
        pieceCol +=2;
    }
}

//TODO fix memory error for out of bounds
void TetrisGameLogic::gameLoop()
{
    int count = 0;//TODO WRITE END CASE FOR WHILE LOOP
    unsigned int time = 100000;
    
    initGameGrid();
    initPieceLocation();
    deletePiece();
    while(count < 10)
    {
        usleep(time);
        verticalPieceMove();
        m_renderer->printGrid();
        count++; 
    }
    system("clear");
}






