//
//

#include <iostream>
#include <stdlib.h>
#include "TetrisUserInput.h"
#ifndef TETRIS_CONSOLE_USER_INPUT_H
#define TETRIS_CONSOLE_USER_INPUT_H
using namespace std;


class TetrisConsoleUserInput : public TetrisUserInput {
public:
   TetrisConsoleUserInput( class TetrisGameLogic& gameLogic );   // Constructor.

private:

};

#endif
