//abstract UserInput class
//used by TetrisConsoleUserInput and TetrisAndroidUserInput

#include <iostream>
#include <stdlib.h>
#ifndef TETRIS_USER_INPUT_H
#define TETRIS_USER_INPUT_H
using namespace std;


class TetrisUserInput{

public:
   TetrisUserInput(class TetrisGameLogic& gameLogic);   // Constructor.
 
protected:
   class TetrisGameLogic& m_gameLogic;

};



#endif
