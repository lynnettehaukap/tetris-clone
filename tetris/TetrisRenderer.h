//abstract rendering class
//used by tetrisConsoleRenderer and TetrisAndroidRenderer

#include <iostream>
#include <stdlib.h>
#ifndef TETRIS_RENDERER_H
#define TETRIS_RENDERER_H
using namespace std;


class TetrisRenderer {
public:
   TetrisRenderer( class TetrisGameLogic& gameLogic);   // Constructor.
   virtual void printGrid() = 0;
protected:
   class TetrisGameLogic& m_gameLogic;

};

#endif
